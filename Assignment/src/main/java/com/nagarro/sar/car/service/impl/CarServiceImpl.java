package com.nagarro.sar.car.service.impl;

import java.util.Map;

import org.springframework.stereotype.Service;

import com.nagarro.sar.car.Car;
import com.nagarro.sar.car.factory.CarFactory;
import com.nagarro.sar.car.service.ICarService;

@Service
public class CarServiceImpl implements ICarService {

	public Map<String, String> getModelFeatures(String carType) {
		Car carModel = CarFactory.getCarInstance(carType);
		Map<String, String> modelFeatures = null;
		if (carModel != null) {
			modelFeatures = carModel.getModelFeatures();
		}
		return modelFeatures;
	}

}
