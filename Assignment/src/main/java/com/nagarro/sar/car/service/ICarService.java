package com.nagarro.sar.car.service;

import java.util.Map;

public interface ICarService {

	Map<String, String> getModelFeatures(String carType);
}
