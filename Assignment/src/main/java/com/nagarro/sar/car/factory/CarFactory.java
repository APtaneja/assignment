package com.nagarro.sar.car.factory;

import java.util.Map;

import com.nagarro.sar.car.Car;
import com.nagarro.sar.car.impl.Ecosport;
import com.nagarro.sar.car.impl.Endeavour;
import com.nagarro.sar.car.impl.Figo;
import com.nagarro.sar.car.impl.Mondeo;
import com.nagarro.sar.enums.CarTypeEnum;

public class CarFactory {

	public static Car getCarInstance(String carType) {
		Car model = null;
		if (carType != null) {
			CarTypeEnum carTypeEnum = CarTypeEnum.valueOf(carType.toUpperCase());
			switch(carTypeEnum) {
				case ECOSPORT :
					model = new Ecosport();
					break;
				case FIGO :
					model = new Figo();
					break;
				case ENDEAVOUR :
					model = new Endeavour();
					break;
				case MONDEO :
					model = new Mondeo();
					break;
			}
		}
		return model;
	}
}
