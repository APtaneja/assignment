package com.nagarro.sar.car;

import java.util.HashMap;
import java.util.Map;

public interface Car {

	Map<String, String> modelFeatures = new HashMap<String, String>();
	
	Map<String, String> getModelFeatures();
}
