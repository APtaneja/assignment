package com.nagarro.sar.car.impl;

import java.util.Map;

import com.nagarro.sar.car.Car;

public class Mondeo implements Car {

	public Map<String, String> getModelFeatures() {
		modelFeatures.put("videoScreening", "No");
		modelFeatures.put("Pen detectable", "Yes");
		return modelFeatures;
	}

}
