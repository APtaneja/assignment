package com.nagarro.sar.car.impl;

import java.util.Map;

import com.nagarro.sar.car.Car;

public class Endeavour implements Car {

	public Map<String, String> getModelFeatures() {
		modelFeatures.put("videoScreening", "No");
		modelFeatures.put("Pen detectable", "No");
		return modelFeatures;
	}

}
