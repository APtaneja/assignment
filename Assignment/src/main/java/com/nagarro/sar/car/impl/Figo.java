package com.nagarro.sar.car.impl;

import java.util.Map;

import com.nagarro.sar.car.Car;

public class Figo implements Car {

	public Map<String, String> getModelFeatures() {
		modelFeatures.put("videoScreening", "Yes");
		modelFeatures.put("Pen detectable", "No");
		return modelFeatures;
	}

}
