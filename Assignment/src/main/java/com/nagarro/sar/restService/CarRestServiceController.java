package com.nagarro.sar.restService;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nagarro.sar.car.service.ICarService;

@RestController
public class CarRestServiceController {

	@Autowired
	ICarService carService;
	
	@RequestMapping(value = "/car/{model}/features", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> getcarModelFeatures(@PathVariable("model") String model) {
		return carService.getModelFeatures(model);
	}
}
