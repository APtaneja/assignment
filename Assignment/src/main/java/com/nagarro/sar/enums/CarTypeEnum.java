package com.nagarro.sar.enums;

public enum CarTypeEnum {

	ECOSPORT,
	
	FIGO,
	
	ENDEAVOUR,
	
	MONDEO;
}
