package com.nagarro.sar.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.nagarro.sar.car.service.ICarService;

@Controller
public class ApplicationController {
	
	@Autowired
	private ICarService carService;

	@RequestMapping("/")
	public String displayPage() {
		return "modelInput";
	}
	
	@RequestMapping("model")
	public String getModelInformation(@RequestParam("modelName") String modelType, Model model) {
		Map<String, String> modelFeatures = carService.getModelFeatures(modelType);
		model.addAttribute("modelData", modelFeatures);
		return "modelFeatures";
	}
}
